package pl.bettersolutions;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.TaskScheduler;
import pl.bettersolutions.config.SchedulerConfigurationProperties;
import pl.bettersolutions.shares.history.SharesChangeChecker;

@RequiredArgsConstructor
@SpringBootApplication
public class BetterSolutionsTaskApplication implements CommandLineRunner {
	private final SharesChangeChecker sharesChangeChecker;
	private final TaskScheduler taskScheduler;
	private final SchedulerConfigurationProperties schedulerConfigurationProperties;
	
	@Override
	public void run(String... args) {
		taskScheduler.scheduleAtFixedRate(
			sharesChangeChecker::checkForSharePriceChanges,
			schedulerConfigurationProperties.getPeriodInMillis()
		);
	}
	
	public static void main(String[] args) {
		SpringApplication.run(BetterSolutionsTaskApplication.class, args);
	}
}
