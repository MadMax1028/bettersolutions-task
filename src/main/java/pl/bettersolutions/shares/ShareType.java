package pl.bettersolutions.shares;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ShareType {
	WIG("WIG", "wig"),
	WIG20("WIG20", "wig20"),
	WIG20_Fut("WIG20 Fut", "fw20"),
	mWIG40("mWIG40", "mwig40"),
	sWIG80("sWIG80", "swig80");
	
	private final String canonicalName, stooqName;
}
