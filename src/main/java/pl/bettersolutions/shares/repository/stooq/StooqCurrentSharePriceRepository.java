package pl.bettersolutions.shares.repository.stooq;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.java.Log;
import lombok.val;
import org.apache.commons.csv.CSVFormat;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;
import pl.bettersolutions.shares.ShareType;
import pl.bettersolutions.shares.repository.CurrentSharePriceRepository;

import java.io.StringReader;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.logging.Level;

@Log
@RequiredArgsConstructor
@Component
public class StooqCurrentSharePriceRepository implements CurrentSharePriceRepository {
	enum Headers {
		Symbol, Data, Czas, Otwarcie, Najwyzszy, Najnizszy, Zamkniecie, Wolumen, LOP
	}
	
	static final String urlFormat = "http://stooq.pl/q/l/?h&e=csv&s=%s";
	static final CSVFormat csvFormat = CSVFormat.DEFAULT.withHeader(Headers.class).withSkipHeaderRecord();
	
	private final RestOperations restOperations;
	
	@SneakyThrows
	@Override
	public BigDecimal getCurrentSharePrice(ShareType shareType) {
		val url = buildUrlForShareType(shareType);
		try {
			val responseBody = restOperations.getForObject(url, String.class);
			val csvParser = csvFormat.parse(new StringReader(responseBody));
			val records = csvParser.getRecords();
			if (records.size() != 1) {
				throw new ParseException(String.format("CSV file contents: '%s', should contain only 1 record.", responseBody), 0);
			}
			val csvRecord = records.get(0);
			
			return new BigDecimal(csvRecord.get(Headers.Zamkniecie));
		} catch (Exception e) {
			logger.log(Level.SEVERE, "An error occurred while parsing '" + url + "'.", e);
			throw e;
		}
	}
	
	String buildUrlForShareType(ShareType shareType) {
		return String.format(urlFormat, shareType.getStooqName());
	}
}
