package pl.bettersolutions.shares.repository;

import pl.bettersolutions.shares.ShareType;

import java.math.BigDecimal;

public interface CurrentSharePriceRepository {
	BigDecimal getCurrentSharePrice(ShareType shareType);
}