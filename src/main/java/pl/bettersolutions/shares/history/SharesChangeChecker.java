package pl.bettersolutions.shares.history;

import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import lombok.val;
import one.util.streamex.StreamEx;
import org.jooq.lambda.Unchecked;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.stereotype.Component;
import pl.bettersolutions.shares.ShareType;
import pl.bettersolutions.shares.repository.CurrentSharePriceRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Log
@RequiredArgsConstructor
@Component
public class SharesChangeChecker {
	private static final ShareType[] shareTypes = ShareType.values();
	
	private final CurrentSharePriceRepository currentSharePriceRepository;
	private final SharesHistoryLogger sharesHistoryLogger;
	private final AsyncTaskExecutor taskExecutor;
	
	private Map<ShareType, BigDecimal> cachedCurrentSharePrices = Collections.emptyMap();
	
	public void checkForSharePriceChanges() {
		val checkDateTime = LocalDateTime.now();
		val sharePrices = getCurrentSharePrices();
		
		logger.info(sharePrices::toString);
		
		logIfPricesChanged(sharePrices, checkDateTime);
	}
	
	private Map<ShareType, BigDecimal> getCurrentSharePrices() {
		val sharePrices = new EnumMap<ShareType, BigDecimal>(ShareType.class);
		StreamEx.of(shareTypes)
			.map(shareType -> taskExecutor.submit(() -> {
				sharePrices.put(shareType, currentSharePriceRepository.getCurrentSharePrice(shareType));
			}))
			.toList()
			.forEach(Unchecked.consumer(task -> task.get(10, TimeUnit.SECONDS)));
		
		return sharePrices;
	}
	
	private synchronized void logIfPricesChanged(Map<ShareType, BigDecimal> sharePrices, LocalDateTime checkDateTime) {
		if (!cachedCurrentSharePrices.equals(sharePrices)) {
			sharesHistoryLogger.logSharePrices(sharePrices, checkDateTime);
			cachedCurrentSharePrices = sharePrices;
		}
	}
}
