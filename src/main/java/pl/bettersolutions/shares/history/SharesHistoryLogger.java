package pl.bettersolutions.shares.history;

import lombok.SneakyThrows;
import lombok.extern.java.Log;
import lombok.val;
import org.apache.commons.csv.CSVFormat;
import org.springframework.stereotype.Component;
import pl.bettersolutions.config.SharesHistoryLoggerProperties;
import pl.bettersolutions.shares.ShareType;

import java.io.File;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Stream;

@Log
@Component
public class SharesHistoryLogger {
	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.BASIC_ISO_DATE;
	private static final Supplier<DecimalFormat> currencyFormatSupplier = () -> new DecimalFormat("0.00");
	private static final CSVFormat csvFormat = CSVFormat.DEFAULT.withDelimiter('|');
	private static final ShareType[] shareTypes = ShareType.values();
	private static final String[] headers = Stream.concat(
		Stream.of("Updated"), Stream.of(shareTypes).map(ShareType::getCanonicalName)
	).toArray(String[]::new);
	
	private final String logLocation;
	private final String logFilePrefix;
	
	public SharesHistoryLogger(SharesHistoryLoggerProperties sharesHistoryLoggerProperties) {
		logLocation = sharesHistoryLoggerProperties.getLogLocation();
		logFilePrefix = sharesHistoryLoggerProperties.getLogFilePrefix();
	}
	
	@SneakyThrows
	public synchronized void logSharePrices(Map<ShareType, BigDecimal> sharePrices, LocalDateTime checkDateTime) {
		if (sharePrices.size() != shareTypes.length) {
			throw new IllegalArgumentException("Prices for all " + shareTypes.length + " share types need to be passed. Given: " + sharePrices.keySet());
		}
		
		val logFile = buildLogFile(checkDateTime);
		
		val printHeaders = !logFile.exists(); // some better method to acquire read/write lock should be applied
		
		try (val logFileWriter = new FileWriter(logFile, true)) {
			val csvPrinter = csvFormat.print(logFileWriter);
			val currencyFormat = currencyFormatSupplier.get();
			
			// headers (only for new log files)
			if (printHeaders) {
				csvPrinter.printRecord((Object[]) headers);
			}
			
			// record
			csvPrinter.print(checkDateTime);
			for (val shareType : shareTypes) {
				csvPrinter.print(currencyFormat.format(sharePrices.get(shareType)));
			}
			csvPrinter.println();
		}
	}
	
	File buildLogFile(LocalDateTime checkDateTime) {
		return new File(logLocation, buildLogFileName(checkDateTime));
	}
	
	private String buildLogFileName(LocalDateTime checkDateTime) {
		return logFilePrefix + dateTimeFormatter.format(checkDateTime) + ".csv";
	}
}