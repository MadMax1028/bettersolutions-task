package pl.bettersolutions.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter // required by Spring
@Configuration
@ConfigurationProperties("shareChecker.scheduler")
public class SchedulerConfigurationProperties {
	private long periodInMillis;
}
