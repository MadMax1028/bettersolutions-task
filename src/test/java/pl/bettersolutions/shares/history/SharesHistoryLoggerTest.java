package pl.bettersolutions.shares.history;

import one.util.streamex.StreamEx;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import pl.bettersolutions.config.SharesHistoryLoggerProperties;
import pl.bettersolutions.shares.ShareType;

import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Map;

import static java.util.function.Function.identity;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class SharesHistoryLoggerTest {
	private static final String logLocation = System.getProperty("java.io.tmpdir");
	private static final String logFilePrefix = SharesHistoryLoggerTest.class.getSimpleName() + "_";
	
	private final SharesHistoryLoggerProperties loggerProperties = new SharesHistoryLoggerProperties();
	{
		loggerProperties.setLogLocation(logLocation);
		loggerProperties.setLogFilePrefix(logFilePrefix);
	}
	
	private final SharesHistoryLogger logger = new SharesHistoryLogger(loggerProperties);
	
	private File logFile;
	
	@AfterMethod
	public void deleteLogFile() {
		if (logFile != null && logFile.exists()) {
			logFile.delete();
			logFile = null;
		}
	}
	
	@Test
	public void shouldCreateNewLogFileIfItDoesntExistAndAppendSharePrices() throws Exception {
		// given
		LocalDateTime checkDateTime = LocalDateTime.of(2000, Month.JANUARY, 1, 0, 0);
		logFile = logger.buildLogFile(checkDateTime);
		
		assertThat(logFile).doesNotExist();
		
		// and
		BigDecimal sharePrice = new BigDecimal("12.34");
		Map<ShareType, BigDecimal> sharePrices = buildPricesForAllShareTypes(sharePrice);
		
		// when
		logger.logSharePrices(sharePrices, checkDateTime);
		
		// then
		assertThat(logFile).exists();
		System.out.println(new String(Files.readAllBytes(logFile.toPath())));
	}
	
	@Test
	public void shouldThrowIllegalArgumentExceptionIfNotAllSharePricesHaveBeenPassed() {
		// given
		LocalDateTime checkDateTime = LocalDateTime.of(2000, Month.JANUARY, 1, 0, 0);
		BigDecimal sharePrice = new BigDecimal("12.34");
		Map<ShareType, BigDecimal> sharePrices = buildPricesForAllShareTypes(sharePrice);
		
		sharePrices.remove(ShareType.WIG);
		
		// expect
		assertThatThrownBy(() -> logger.logSharePrices(sharePrices, checkDateTime))
			.isInstanceOf(IllegalArgumentException.class)
			.hasNoCause();
	}
	
	private Map<ShareType, BigDecimal> buildPricesForAllShareTypes(BigDecimal sharePrice) {
		return StreamEx.of(ShareType.values()).toMap(identity(), shareType -> sharePrice);
	}
}