package pl.bettersolutions.shares.history;

import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.annotation.AsyncResult;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.bettersolutions.shares.repository.CurrentSharePriceRepository;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class SharesChangeCheckerTest {
	private CurrentSharePriceRepository currentSharePriceRepository;
	private SharesHistoryLogger sharesHistoryLogger;
	private AsyncTaskExecutor taskExecutor;
	
	private SharesChangeChecker sharesChangeChecker;
	
	@BeforeMethod
	public void initializeTest() {
		currentSharePriceRepository = mock(CurrentSharePriceRepository.class);
		sharesHistoryLogger = mock(SharesHistoryLogger.class);
		taskExecutor = mock(AsyncTaskExecutor.class);
		{
			when(taskExecutor.submit(any(Runnable.class))).then(invocation -> {
				invocation.<Runnable>getArgument(0).run();
				return new AsyncResult<>(null);
			});
		}
		
		sharesChangeChecker = new SharesChangeChecker(currentSharePriceRepository, sharesHistoryLogger, taskExecutor);
	}
	
	@Test
	public void shouldInvokeLoggerOnlyOnceOnFirstUsageIfPricesHaveNotChangedLater() {
		// given
		BigDecimal sharePrice = BigDecimal.TEN;
		
		when(currentSharePriceRepository.getCurrentSharePrice(any())).thenReturn(sharePrice);
		
		// when
		sharesChangeChecker.checkForSharePriceChanges();
		sharesChangeChecker.checkForSharePriceChanges();
		
		// then
		verify(sharesHistoryLogger).logSharePrices(any(), any());
	}
	
	@Test
	public void shouldInvokeLoggerIfPricesHaveChanged() {
		// given
		BigDecimal initialSharePrice = BigDecimal.TEN;
		BigDecimal newSharePrice = BigDecimal.ZERO;
		
		// when
		when(currentSharePriceRepository.getCurrentSharePrice(any())).thenReturn(initialSharePrice);
		sharesChangeChecker.checkForSharePriceChanges(); // first usage
		
		// and
		when(currentSharePriceRepository.getCurrentSharePrice(any())).thenReturn(newSharePrice);
		sharesChangeChecker.checkForSharePriceChanges();
		
		// then
		verify(sharesHistoryLogger, times(2)).logSharePrices(any(), any());
	}
}