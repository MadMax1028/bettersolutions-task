package pl.bettersolutions.shares.repository.stooq;

import org.apache.commons.csv.CSVPrinter;
import org.springframework.web.client.RestOperations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.bettersolutions.shares.ShareType;

import java.math.BigDecimal;
import java.net.URL;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StooqCurrentSharePriceRepositoryTest {
	private RestOperations restOperations;
	
	private StooqCurrentSharePriceRepository repository;
	
	@BeforeMethod
	public void initializeTest() {
		restOperations = mock(RestOperations.class);
		
		repository = new StooqCurrentSharePriceRepository(restOperations);
	}
	
	@Test
	public void shouldReturnSharePriceWhenCSVContainsOneRecord() {
		// given
		final BigDecimal expectedSharePrice = new BigDecimal("12.23");
		final int numberOfRecords = 1;
		
		mockReturnedCSVFile(expectedSharePrice, numberOfRecords);
		
		// when
		BigDecimal sharePrice = repository.getCurrentSharePrice(ShareType.WIG20);
		
		// then
		assertThat(sharePrice).isEqualTo(expectedSharePrice);
	}
	
	@DataProvider(name = "invalidNumbersOfRecords")
	public static Object[][] provideInvalidNumbersOfRecords () {
		return new Object[][] {{0}, {2}};
	}
	
	@Test(dataProvider = "invalidNumbersOfRecords")
	public void shouldThrowParseExceptionWhenCSVDoesntContainOneRecord(int numberOfRecords) {
		// given
		final BigDecimal expectedSharePrice = new BigDecimal("12.23");
		
		mockReturnedCSVFile(expectedSharePrice, numberOfRecords);
		
		// expect
		assertThatThrownBy(() -> repository.getCurrentSharePrice(ShareType.WIG20))
			.isInstanceOf(ParseException.class)
			.hasNoCause();
	}
	
	@DataProvider(name = "shareTypes")
	public static Iterator<Object[]> provideShareTypes () {
		return Stream.of(ShareType.values()).map(shareType -> new Object[] { shareType }).iterator();
	}
	
	@Test(dataProvider = "shareTypes")
	public void shouldBuildCorrectUrlToShareDataOnStooqService(ShareType shareType) {
		String expectedUrl = String.format(StooqCurrentSharePriceRepository.urlFormat, shareType.getStooqName());
		
		// when
		String url = repository.buildUrlForShareType(shareType);
		
		// then
		assertThat(url).isEqualTo(expectedUrl);
		assertThat(catchThrowable(() -> new URL(url))).isNull();
	}
	
	private void mockReturnedCSVFile(BigDecimal sharePrice, int numberOfRecords) {
		when(restOperations.getForObject(anyString(), same(String.class))).then(invocation -> {
			StringBuilder sb = new StringBuilder();
			CSVPrinter csvPrinter = StooqCurrentSharePriceRepository.csvFormat.print(sb);
			
			StooqCurrentSharePriceRepository.Headers[] headers = StooqCurrentSharePriceRepository.Headers.values();
			int sharePriceOrdinalPosition = StooqCurrentSharePriceRepository.Headers.Zamkniecie.ordinal();
			
			// header
			csvPrinter.printRecord(headers);
			
			// records
			String[] csvRecordContents = new String[headers.length];
			Arrays.fill(csvRecordContents, "1234");
			csvRecordContents[sharePriceOrdinalPosition] = sharePrice.toString();
			
			for(int i = 0; i < numberOfRecords; i++) {
				csvPrinter.printRecord(csvRecordContents);
			}
			
			return sb.toString();
		});
	}
}